import React from 'react';
import { StyleSheet, Text, ScrollView, View, Button, Image, Dimensions, PixelRatio, Alert } from 'react-native';
import { takeSnapshotAsync, Print } from 'expo';

export default class App extends React.Component {
    view;
    state = {
        imageURL: ''
    }
    onScreenshot = async () => {
        const pdf = await this.generatePDF()
        return Print.printAsync({ uri: pdf.uri }).catch(error =>
            Alert.alert(error.message)
        );
    }
    generatePDF = async () => {

        const { height, width } = Dimensions.get("window");

        const targetPixelCount = 1080;
        const pixelRatio = PixelRatio.get()
        const pixels = targetPixelCount / pixelRatio;

        let snapshot = await takeSnapshotAsync(this.view)
        if (snapshot.startsWith('/')) {
            snapshot = 'file://' + snapshot
        }
        this.setState({
            imageURL: snapshot,
            height: pixels,
            width: pixels
        })

        let html = `<img src=${snapshot} width="100%" style="max-height:100%" />`;
        return Print.printToFileAsync({ html });
    }
    render() {

        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View ref={component => this.view = component}>
                    <Text>Open up App.js to start working on your app!</Text>
                    <Button onPress={this.onScreenshot} title="Take a screenshot of this view" />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Img />
                    <Text>Hello</Text>
                </View>
            </ScrollView>
        );
    }
}

const Img = () => <Image style={{ width: 200, height: 130 }} source={{ uri: 'https://www.super-som.com/static/media/super-cloud-rgb.a9a3896c.png' }} />

const styles = StyleSheet.create({
    container: {

        // backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'flex-start',
        // flexDirection: 'column',
        // borderColor: 'black',
        // borderWidth: 5,
        //height: 300
    },
});
